import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { User } from "src/app/entiteti";

@Component({
  selector: "app-korisnici-izmena",
  templateUrl: "./korisnici-izmena.component.html",
  styleUrls: ["./korisnici-izmena.component.css"],
})
export class KorisniciIzmenaComponent implements OnInit {
  @Output() onSubmit: EventEmitter<User> = new EventEmitter();

  ngOnInit(): void {}

  @Input()
  updateUser: User;

  azuriraj() {
    this.onSubmit.emit(this.updateUser);
  }
}
