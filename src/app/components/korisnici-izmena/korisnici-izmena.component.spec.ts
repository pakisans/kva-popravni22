import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KorisniciIzmenaComponent } from './korisnici-izmena.component';

describe('KorisniciIzmenaComponent', () => {
  let component: KorisniciIzmenaComponent;
  let fixture: ComponentFixture<KorisniciIzmenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KorisniciIzmenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KorisniciIzmenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
