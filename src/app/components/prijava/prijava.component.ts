import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "src/app/entiteti";
import { PrijavaService } from "src/app/services/prijava.service";

@Component({
  selector: "app-prijava",
  templateUrl: "./prijava.component.html",
  styleUrls: ["./prijava.component.css"],
})
export class PrijavaComponent implements OnInit {
  constructor(private prijavaService: PrijavaService, private router: Router) {}

  ngOnInit(): void {}

  user: User = {
    id: null,
    username: "",
    password: "",
  };

  prijavi() {
    this.prijavaService.login(this.user).subscribe((res) => {
      this.router.navigate(["/users"]);
    });
  }
}
