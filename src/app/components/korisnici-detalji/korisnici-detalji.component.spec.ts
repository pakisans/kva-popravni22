import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KorisniciDetaljiComponent } from './korisnici-detalji.component';

describe('KorisniciDetaljiComponent', () => {
  let component: KorisniciDetaljiComponent;
  let fixture: ComponentFixture<KorisniciDetaljiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KorisniciDetaljiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KorisniciDetaljiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
