import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-korisnici-detalji",
  templateUrl: "./korisnici-detalji.component.html",
  styleUrls: ["./korisnici-detalji.component.css"],
})
export class KorisniciDetaljiComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input()
  password: string;
}
