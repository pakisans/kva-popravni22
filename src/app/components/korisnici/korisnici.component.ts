import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { User } from "src/app/entiteti";
import { PrijavaService } from "src/app/services/prijava.service";

@Component({
  selector: "app-korisnici",
  templateUrl: "./korisnici.component.html",
  styleUrls: ["./korisnici.component.css"],
})
export class KorisniciComponent implements OnInit {
  constructor(
    public prijavaService: PrijavaService,
    private http: HttpClient
  ) {}

  users: User[];

  prikaziFormu: boolean = false;

  ngOnInit(): void {
    this.dobaviKorisnike();
  }

  selectedUser: User;

  pass: string;

  dobaviKorisnike() {
    this.http
      .get<User[]>(`http://localhost:8080/api/users`)
      .subscribe((res) => (this.users = res));
  }

  obrisi(id: number) {
    this.http
      .delete(`http://localhost:8080/api/users/${id}`)
      .subscribe((res) => {
        this.dobaviKorisnike();
      });
  }

  azuriraj(user: User, id: number) {
    this.prikaziFormu = !this.prikaziFormu;
    this.selectedUser = user;
    this.selectedUser.id = id;
  }

  azurirajKorisnika(user: User) {
    this.http
      .put(`http://localhost:8080/api/users/${user.id}`, user)
      .subscribe((res) => {
        this.prikaziFormu = false;
        this.dobaviKorisnike();
      });
  }

  detalji(password: string) {
    this.pass = password;
  }
}
