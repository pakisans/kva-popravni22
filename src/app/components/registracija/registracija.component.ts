import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "src/app/entiteti";
import { RegistracijaService } from "src/app/services/registracija.service";

@Component({
  selector: "app-registracija",
  templateUrl: "./registracija.component.html",
  styleUrls: ["./registracija.component.css"],
})
export class RegistracijaComponent implements OnInit {
  constructor(
    private registracijaService: RegistracijaService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  user: User = {
    id: null,
    username: "",
    password: "",
    roles: ["ROLE_USER"],
  };

  registruj() {
    this.registracijaService.register(this.user).subscribe((res) => {
      console.log(res);
      this.router.navigate(["users"]);
    });
  }
}
