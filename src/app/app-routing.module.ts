import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { KorisniciComponent } from "./components/korisnici/korisnici.component";
import { PrijavaComponent } from "./components/prijava/prijava.component";
import { RegistracijaComponent } from "./components/registracija/registracija.component";

const routes: Routes = [
  {
    path: "korisnici",
    component: KorisniciComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ["ROLE_ADMIN", "ROLE_USER"],
    },
  },
  {
    path: "prijava",
    component: PrijavaComponent,
  },
  {
    path: "registracija",
    canActivate: [AuthGuard],
    component: RegistracijaComponent,
    data: {
      allowedRoles: ["ROLE_ADMIN"],
    },
  },
  {
    path: "**",
    component: KorisniciComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
