export interface User {
  id: number;
  username: string;
  password: string;
  roles?: string[];
}
export interface File {
  id: number;
  title: string;
  type: string;
  directory: string;
  size: number;
}
