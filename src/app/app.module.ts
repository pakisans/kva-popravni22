import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { KorisniciDetaljiComponent } from "./components/korisnici-detalji/korisnici-detalji.component";
import { KorisniciIzmenaComponent } from "./components/korisnici-izmena/korisnici-izmena.component";
import { KorisniciComponent } from "./components/korisnici/korisnici.component";
import { PrijavaComponent } from "./components/prijava/prijava.component";
import { RegistracijaComponent } from "./components/registracija/registracija.component";
import { InterceptorService } from "./interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    KorisniciComponent,
    KorisniciDetaljiComponent,
    RegistracijaComponent,
    PrijavaComponent,
    KorisniciIzmenaComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
