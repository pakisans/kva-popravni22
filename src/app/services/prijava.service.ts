import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "../entiteti";

@Injectable({
  providedIn: "root",
})
export class PrijavaService {
  token: any;
  constructor(private http: HttpClient) {}

  private user: User = {
    id: null,
    username: "",
    password: "",
    roles: [],
  };

  login(user): Observable<User> {
    return this.http
      .post<User>(`http://localhost:8080/api/login`, user)
      .pipe((res) => {
        res.subscribe((token) => {
          this.token = token["token"];
          this.user = JSON.parse(atob(token["token"].split(".")[1]));
        });

        return res;
      });
  }

  validateRoles(roles) {
    if (this.user) {
      let userRoles: Set<string> = new Set(this.user.roles);
      let matchedRoles = roles.filter((r) => userRoles.has(r));
      if (matchedRoles.length > 0) {
        return true;
      }
    }
    return false;
  }

  getToken() {
    if (!this.token) {
      return "";
    }
    return this.token;
  }
}
