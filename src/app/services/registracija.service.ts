import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "../entiteti";

@Injectable({
  providedIn: "root",
})
export class RegistracijaService {
  constructor(private http: HttpClient) {}

  register(user: User): Observable<User> {
    return this.http.post<User>(`http://localhost:8080/api/users`, user);
  }
}
